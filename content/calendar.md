---
Title: Events
index: 2
---

<div style="position: relative; overflow: hidden; width: 100%; padding-top: 100%">
<script>
window.onload = function changeUrl() {
  var d = new Date ();
  var newSrc = "https://parich.us/nextcloud/index.php/apps/calendar/embed/zFL5eoGZZ5fQwxWt/listMonth/" + d.toISOString().split('T')[0];
  console.log(newSrc);

  document.getElementById('calendar_frame').src=newSrc;
}
</script>
<iframe id="calendar_frame" style="position: absolute; top:0; left:0; bottom:0; right:0; width: 100%; height: 100%;" src=""></iframe>
</div>
