---
Title: Subscribe
index: 3
---

<form method="post" action="https://scripts.dreamhost.com/add_list.cgi">

<input type="hidden" name="list" value="announce">
<input type="hidden" name="domain" value="hub-city.tech">

<div>
<label for="email">Email:</label>
<input type="text" name="email" placeholder="Enter a valid email" required>
</div>

<div>
<input type="submit" name="submit" value="Join Our Announcement List">
<input class="button" type="submit" name="unsub" value="Remove from list">
</div>
