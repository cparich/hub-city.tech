---
Title: October Update
index: 0
---

October is finally here and that means we'll be kicking off Hub City Tech!

###Thoughts

October brings with it some of the first cool, enjoyable weather; along with the starts of deer, squirrel, rabbit, and dove season.
And while some of us will have our allergies assaulted by the upcoming foggy mornings and burning cane fields, that won't diminish the joy of finally getting some sweater weather.

###Events

 * Hub City Tech is hosting a BBQ for all interested: October 16th at noon, Girard Park! BYO, some refreshments will be provided.
 * International Gumbo Cookoff is the weekend of October 9th-10th in New Iberia Bouligny Plaza. Expect $10-$20 per head.

###Closing

Hub City Tech is all about fellowship. Providing opportunities for people with a common interest (tech) to gather and meet and communicate is our first goal.
We have a common chat room bridged across multiple common services for announcements, discussion, support, etc. See the bottom of our main page for links.
