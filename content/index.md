---
stellar:
  description: Acadiana Region

footer:
  title: Hub City Technology Club
  actions:
    E-Mail:
      icon: fa-envelope
      url: mailto:admin@hub-city.tech
    Matrix:
      icon: assets/matrix-logo.svg
      url: https://matrix.to/#/#hub-city.tech:parish.ems.host
    Telegram:
      icon: fa-telegram
      url: https://t.me/hubcitytech
      options: brands
    Discord:
      icon: fa-discord
      url: https://discord.gg/5ZdZA3dhTx
      options: brands
    Nextcloud Chat:
      url: https://parich.us/nextcloud/index.php/call/kxxu2xms
---
