---
Title: About
index: 1
mode: spotlight
image: assets/1.jpeg
---

Hub City Tech is an Acadiana-based community group focused on software and technology.

Our mission is to bring people together for knowledge and comradery, to let them explore their 
career in the presence of knowledgable mentors and skilled peers.

